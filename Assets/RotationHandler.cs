﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationHandler : MonoBehaviour {

    private Draggable dragComponent;

    // Use this for initialization
    void Start () {
        dragComponent = GetComponent<Draggable>();

        dragComponent.OnDrag += RotateObject;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void RotateObject(object target, DragEventArgs eventArgs)
    {
        float angleX = 0;
        float angleY = 0;
        float angleZ = 0;

        Vector3 cursorTranslation = eventArgs.CursorTranslation;
        Vector3 handlerPosition = eventArgs.GameObjectOnCursorOnStart.transform.localPosition;
        Vector3 direction = (handlerPosition - Vector3.zero).normalized;

        Debug.Log(direction);

        // Slect which axis we are rotating now
        if (handlerPosition.z != 0)
        {
            if (handlerPosition.x != 0)
                angleX = cursorTranslation.x * -direction.z;

            if (handlerPosition.y != 0)
                angleY = cursorTranslation.y * -direction.z;
        }
        else
            angleZ = cursorTranslation.y * direction.x;

        transform.parent.Rotate(angleY * 100, -angleX * 100, angleZ * 100);
    }

    void Destroy()
    {
        dragComponent.OnDrag -= RotateObject;
    }
}
