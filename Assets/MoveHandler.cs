﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveHandler : MonoBehaviour {

    private Draggable dragComponent;

	// Use this for initialization
	void Start () {
        dragComponent = GetComponent<Draggable>();

        dragComponent.OnDrag += MoveObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void MoveObject(object target, DragEventArgs eventArgs)
    {
        transform.parent.Translate(eventArgs.CursorTranslation);
    }

    void Destroy()
    {
        dragComponent.OnDrag -= MoveObject;
    }
}
