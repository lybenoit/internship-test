﻿public enum DragState {
    STARTED,
    UPDATED,
    ENDED
}
